#!/usr/bin/env bash

export FLASK_APP=./app/index.py
source ../venv/bin/activate
flask run -h 0.0.0.0