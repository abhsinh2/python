# Commands
```
$ virtualenv venv
$ source venv/bin/activate
$ pip install flask flask-jsonpify flask-sqlalchemy flask-restful marshmallow
$ pip freeze
```

# Curl command
```
curl -X POST -H "Content-Type: application/json" -d '{
  "id": 3,
  "name": "ravi"
}' http://localhost:5000/users
```

# build the image
```
docker build -t users_command_service .
```

# run a new docker container named cashman
```
docker run --name users_command_service -d -p 5000:5000 users_command_service
```

# Run server
```
python runServer.py 
```

# Run command line
```
python runCommand.py -s 127.0.0.1 -p 5000
python runCommand.py -s 127.0.0.1 -p 5000 --username admin --password python
```

# Commands with self prompt
```
users
users --id 1
users -c "{'name':'abhi'}"
users -u "{'id': 2, 'name': 'Laxman'}"
users -d 1
```

# Commands withpour self prompt
```
./runCommand users
./runCommand users --id 1
./runCommand users -c "{'name':'abhi'}"
./runCommand users -u "{'id': 2, 'name': 'Laxman'}"
./runCommand users -d 1
```

