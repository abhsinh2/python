#!venv/bin/python

from app.server import app

def main():
  app.run()

if __name__ == "__main__":
  main()
