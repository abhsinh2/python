#!venv/bin/python

from app.command import main as commandMain

def main():
  commandMain()

if __name__ == '__main__':
  main();