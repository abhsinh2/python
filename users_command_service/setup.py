from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

setup(
  name='user_command_service',
  version='1.2.0',
  description='Access user service',
  url='https://github.com/pypa/sampleproject',
  author='Abhishek Sinha',
  author_email='test@test.com',
  classifiers=[],
  keywords='',
  packages=find_packages(exclude=['contrib', 'docs', 'tests']),
  install_requires=['flask', 'flask_httpauth', 'marshmallow'],
  package_data={},
  entry_points={
    'console_scripts': [
      'users_command_service=app:main',
    ],
  },
  project_urls={}
)