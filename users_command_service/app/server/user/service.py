from .model import User, UserType, UserSchema

class UserService:
  def __init__(self):
    self.USERS = [
      User(1, 'ram', UserType.PERM),
      User(2, 'shyam', UserType.TEMP)
    ]

  def getAll(self):
    schema = UserSchema(many=True)
    users = schema.dump(self.USERS)
    return users.data

  def getById(self, id):
    user = list(filter(lambda u: u.id == id, self.USERS))
    schema = UserSchema(many=False)
    users = schema.dump(user)
    return users.data

  def create(self, userJson):
    id = self.USERS[-1].id + 1

    newUser = User(id, userJson["name"])
    self.USERS.append(newUser)

    schema = UserSchema(many=False)
    user = schema.dump(newUser)
    return user.data

  def update(self, id, userJson):
    user = [user for user in self.USERS if user.id == id]
    if (len(user) != 0):
      user[0].name = userJson['name']
      schema = UserSchema(many=False)
      user = schema.dump(user)
      return user.data
    return None

  def delete(self, id):
    user = [user for user in self.USERS if user.id == id]
    if len(user) == 0:
      return None
    self.USERS.remove(user[0])

    schema = UserSchema(many=False)
    user = schema.dump(user[0])
    return user.data
