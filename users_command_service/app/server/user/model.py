from enum import Enum
from marshmallow import Schema, fields

class User():
  def __init__(self, id, name, type='Temporary'):
    self.id = id
    self.name = name
    self.type = type

class UserType(Enum):
  TEMP = "Temporary"
  PERM = "Permanent"

class UserSchema(Schema):
  id = fields.Int()
  name = fields.Str()
  type = UserType