from flask import jsonify, request, abort, url_for, Blueprint
from .service import UserService
from ...server import auth

profile = Blueprint('users', __name__)

userService = UserService()

def make_public(user):
  new_user = {}
  for field in user:
    if field == 'id':
      new_user[field] = user[field]
      new_user['uri'] = url_for('users.getUser', id=user['id'], _external=True)
    else:
      new_user[field] = user[field]
  return new_user

@profile.route('/', methods=['GET'])
@auth.login_required
def getAll():
  return jsonify({'users': [make_public(user) for user in userService.getAll()]})

@profile.route('/<int:id>', methods=['GET'])
@auth.login_required
def getUser(id):
  user = [user for user in userService.getAll() if user["id"] == id]
  if len(user) == 0:
    abort(404)
  return jsonify({'user': make_public(user[0])})

@profile.route('/', methods=['POST'])
@auth.login_required
def createUser():
  if not request.json or not 'name' in request.json:
    abort(400)

  user = userService.create(request.json)
  return jsonify({'user': make_public(user)}), 201


@profile.route('/<int:id>', methods=['PUT'])
@auth.login_required
def updateUser(id):
  user = [user for user in userService.getAll() if user["id"] == id]

  if len(user) == 0:
    abort(404)
  if not request.json:
    abort(400)
  if 'name' in request.json and type(request.json['name']) != str:
    abort(400)

  user = userService.update(id, request.json)
  return jsonify({'user': make_public(user)}), 201


@profile.route('/<int:id>', methods=['DELETE'])
@auth.login_required
def deleteUser( id):
  user = [user for user in userService.getAll() if user["id"] == id]

  if len(user) == 0:
    abort(404)

  userService.delete(id)
  return jsonify({'result': True})