from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

from flask import jsonify, Flask, make_response
app = Flask(__name__)

from .user.controller import profile

#app.config.from_object('config')
app.register_blueprint(profile, url_prefix='/api/v1.0/users')

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)

@auth.get_password
def get_password(username):
    if username == 'admin':
        return 'python'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)

