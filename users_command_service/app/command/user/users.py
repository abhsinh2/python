import json
import ast

class User:
  def __init__(self, conn, headers):
    self.conn = conn
    self.headers = headers

  def getAll(self):
    self.headers["Content-Type"] = "application/json"
    self.conn.request("GET", "/api/v1.0/users/", headers=self.headers)
    response = self.conn.getresponse()
    print(response.status, response.reason)
    self.__printJson(json.loads(response.read()))

  def getById(self, id):
    self.headers["Content-Type"] = "application/json"
    self.conn.request("GET", "/api/v1.0/users/" + str(id), headers=self.headers)
    response = self.conn.getresponse()
    print(response.status, response.reason)
    self.__printJson(json.loads(response.read()))

  def create(self, data):
    self.headers["Content-Type"] = "application/json"
    dd = ast.literal_eval(data)
    json_data = json.dumps(dd)

    self.conn.request("POST", "/api/v1.0/users/", json_data, headers=self.headers)
    response = self.conn.getresponse()
    print(response.status, response.reason)

  def update(self, data):
    self.headers["Content-Type"] = "application/json"
    dd = ast.literal_eval(data[0])
    json_data = json.dumps(dd)
    self.conn.request("PUT", "/api/v1.0/users/" + str(dd['id']), json_data, headers=self.headers)
    response = self.conn.getresponse()
    print(response.status, response.reason)

  def delete(self, id):
    self.headers["Content-Type"] = "application/json"
    self.conn.request("DELETE", "/api/v1.0/users/" + str(id), headers=self.headers)
    response = self.conn.getresponse()
    print(response.status, response.reason)

  def __printJson(self, data):
    print(json.dumps(data, indent=4, sort_keys=True))

if __name__ == '__main__':
  pass