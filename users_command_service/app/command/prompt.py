import argparse
from cmd import Cmd
import http.client
from base64 import b64encode
import json

from .user import User

class Server:
  def __init__(self, host, port, username, password):
    self.host = host
    self.port = port
    self.username = username
    self.password = password

  def connect(self):
    conn = http.client.HTTPConnection(self.host, self.port)
    return conn

class UserPrompt(Cmd):

  def init(self, server, conn):
    headers = {}
    if (server.username):
      credStr = server.username + ":" + server.password
      userAndPass = b64encode(credStr.encode()).decode("ascii")
      headers['Authorization'] = 'Basic %s' % userAndPass

    self.user = User(conn, headers)

  def help_introduction(self):
    print('Welcome to PDS command line')

  def emptyline(self):
    pass

  def do_EOF(self, line):
    return True

  def do_quit(self, args):
    print("Quitting.")
    raise SystemExit

  def do_users(self, args):
    try:
      options = self._getOptions(args)

      if (options == None):
        self.user.getAll()
      elif (options.createdata != None):
        self.user.create(options.createdata)
      elif (options.updatedata != None):
        self.user.update(options.updatedata)
      elif (options.deleteId != None):
        self.user.delete(options.deleteId)
      elif (options.id != None):
        self.user.getById(options.id)
      elif (options.name != None):
        print('Get user with name', options.name)
      else:
        self.user.getAll()
    except ValueError as err:
      print("Some error:", err)

  def help_users(self):
    print('\n'.join(
      [
        'users -> list users',
        'users -c {} -> create user',
        'users -u {} -> update user',
        'users -d {} -> delete user'
      ]
    ))

  def _getOptions(self, args):
    dt = [];

    if (args.startswith('-c') or args.startswith('-u') or args.startswith('-d')):
      dt.append(args[0:2])
      dt.append(args[3:])
    elif (args.startswith('--create') or args.startswith('--update') or args.startswith('--delete')):
      dt.append(args[0:8])
      dt.append(args[9:])
    elif (args.startswith('--id')):
      dt.append(args[0:4])
      dt.append(args[5:])
    elif (args.startswith('--name')):
      dt.append(args[0:6])
      dt.append(args[7:])

    parser = argparse.ArgumentParser(prog='USERS')

    #parser.add_argument('--version', action='store', dest='version', default="1.0")
    parser.add_argument('-c', '--create', type=json.loads, action="store", dest="createdata", help='Creates a user')
    parser.add_argument('-u', '--update', type=json.loads, action="store", dest="updatedata", help='Updates a user')
    parser.add_argument('-d', '--delete', type=int, action="store", dest="deleteId", help='Delete a user')
    parser.add_argument('--id', type=int, action="store", dest="id", help='Returns user with given id')
    parser.add_argument('--name', action="store", dest="name", help='Returns users with given name')

    options = parser.parse_args(dt)
    return options

def main():
  parser = argparse.ArgumentParser(description='Welcome to PDS server')

  parser.add_argument('-s', '--host', type=str, action="store", dest="host", help="IP to connect")
  parser.add_argument('-p', '--port', type=int, action="store", dest="port", default=8080,
                    help="Port on which application is running. Default is %default")
  parser.add_argument('-u', '--username', type=str, action="store", dest="username", help="username")
  parser.add_argument('--password', type=str, action="store", dest="password", help="password", metavar="PASSWORD")

  options = parser.parse_args()

  if options.host == None:
    parser.error("Host is missing")

  server = Server(options.host, options.port, options.username, options.password)
  conn = server.connect()

  prompt = UserPrompt()
  prompt.init(server, conn)

  prompt.prompt = '> '
  prompt.cmdloop('Starting prompt...')

if __name__ == '__main__':
  pass