import argparse
import http.client
from base64 import b64encode

from .user import User

class Server:
  def __init__(self, host, port, username, password):
    self.host = host
    self.port = port
    self.username = username
    self.password = password

  def connect(self):
    conn = http.client.HTTPConnection(self.host, self.port)
    return conn

server = Server("localhost", 5000, "admin", "python")
conn = server.connect()

class UserPrompt():

  def __init__(self):
    headers = {}
    if (server.username):
      credStr = server.username + ":" + server.password
      userAndPass = b64encode(credStr.encode()).decode("ascii")
      headers['Authorization'] = 'Basic %s' % userAndPass

    self.user = User(conn, headers)

  def request(self, options):
    try:
      if (options.createdata != None):
        self.user.create(options.createdata)
      elif (options.updatedata != None):
        self.user.update(options.updatedata)
      elif (options.deleteId != None):
        self.user.delete(options.deleteId)
      elif (options.id != None):
        self.user.getById(options.id)
      else:
        self.user.getAll()
    except ValueError as err:
      print("Some error:", err)

def main():
  parser = argparse.ArgumentParser(description='PDS CTL')
  parser.add_argument('--version', action='version', version='1.0')

  parser.add_argument('command', type=str, help = 'users CRUD for users etc')

  parser.add_argument('-c', '--create', metavar='Create', type=str, action="store", dest="createdata", help='Create a User')
  parser.add_argument('-u', '--update', metavar='Update', type=str, action="store", dest="updatedata", help='Update a User')
  parser.add_argument('-d', '--delete', metavar='Delete', type=int, action="store", dest="deleteId", help='Delete a User')
  parser.add_argument('--id', type=int, help='Get a User')

  parser.add_argument('--username', type=str, action="store", dest="username", help="username")
  parser.add_argument('--password', type=str, action="store", dest="password", help="password", metavar="PASSWORD")

  options = parser.parse_args()
  print(options)

  userPrompt = UserPrompt()

  if options.command == 'users':
    userPrompt.request(options)
  else:
    print("Invalid command.")

if __name__ == '__main__':
  pass